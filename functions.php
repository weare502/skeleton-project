<?php
// check for Timber
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

// change views directory to templates
Timber::$locations = __DIR__ . '/templates';

class MySite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

		add_action( 'init', function() {
			add_editor_style('style.css' );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Navigation' );

		// Images Sizes
		add_image_size( 'xlarge', 2880, 2000 );
		add_image_size( 'large-square', 500, 500, true );

		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title'	=> 'Options',
			'menu_slug' 	=> 'theme-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	function enqueue_scripts() {
		// Dependencies
		wp_enqueue_style( 'my-css', get_stylesheet_directory_uri() . "/style.css", array(), '20180701' );
		wp_enqueue_script( 'my-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore' ), '20182201' );
		wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
		wp_enqueue_style( 'jquery-ui' );
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Add buttons to the MCE Editor
	function tiny_mce_insert_formats( $init_array ) {

		$style_formats = array(  
			// Each array child is a format with its own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
			),
		);

		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;
	}

	function register_post_types() {
		// add cpts here - example below
		//require 'inc/post-type-story.php';
	}
}

new MySite();

// Primary Nav Menu
function theme_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu',
	) );
}

// Primary Footer Menu
// function theme_render_footer_menu() {
// 	wp_nav_menu( array(
// 		'theme_location' => 'footer',
// 		'container' => false,
// 		// 'menu_class' => '',
// 		'menu_id' => 'footer-menu',
// 	) );
// }