# 502 Starter Theme  
  
### Applications  
1. **VS Code** or **Sublime**  
2. **Codekit 3** *if on Mac* or **Prepros 6** *if on Windows*  
3. **PHP 7.1+**  
4. **Local by Flywheel** to spin up a local server (*nginx + Varnish + PHP 7.1+*)  
	- *Preferred settings should have this by default*  
  
### Plugins  
1. **Timber**  
2. **ACF Pro** (*Advanced Custom Fields Pro*)  
3. **Gravity Forms**  
4. **WP Pusher**  
5. **WP Migrate DB Pro** (*When launching*)  
6. **Any other Plugins the site requires**  
  
### Folder Structure  
1. **inc** folder - used for custom post types  
2. **static** folder - has sub folders for images, javascript, and scss  
3. **templates** folder - used for `.twig` php files  
4. **root** folder *theme root* - contains php base files, tool configs, and the main `style.css` file  
  
### Additional Notes  
- **SCSS: The `typography`, `globals`, and `variables` files will need to be updated for the new theme**  
- **TWIG: The `base` file will need to be updated for the new theme**  
- **PHP: The main php files will need to be updated with any `$context` calls/additions you add**  
- **Media Queries should go inline with the style it's affecting, not all at the bottom/top of your file. This will make things easier to find/fix for everyone.**  
- **The `config.codekit3` file should be mapped properly and should compile scss to the `style.css` file in the *theme root***

### Functions.php  
- **Line 13: `MySite` - rename using the format `ThemeNameSite` - don't use spaces, dashes, or underscores in this name (CamelCase)**  
- **Line 102: `MySite` - rename using the name you chose above. They need to be identical.**  
- **Filters and Actions should be placed within groups of *like-named* items**  
- **Nav Menu's should be registered below line 102**  
- **CPT's should be registered in the function block starting on line 96**  