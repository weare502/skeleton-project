/* global jQuery */
(function($) {
    $(document).ready(mobileMenu); // Mobile Menu
    //$(document).ready(showMore);   // Show More Button Handler

    // Mobile Menu Drop Down
    function mobileMenu() {
        $('#menu-toggle').click(function() {
            $('#primary-menu').toggleClass('menu-open');
        });
    }

    /** 
     * 
     * You will need to edit class names and strings as needed for your project
     * 
    **/


    // "Show More" Button Function
    // function showMore() {
    //     $('#load-more-posts').click(function() {

    //         // Toggle the box class to show it
    //         $('.show-more-boxes').toggleClass('stagger-boxes');

    //         // toggle the button's text
    //         var btn = document.getElementById('load-more-posts');

    //         if( btn.innerHTML === "See More" ) {
    //             btn.innerHTML = "See Less";
    //         } else {
    //             btn.innerHTML = "See More";
    //         }
    //     });
    // }

})(jQuery)