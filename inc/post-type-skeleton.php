<?php

$labels = array(
	'name'               => __( 'CPTNames', 'projectName' ),
	'singular_name'      => __( 'CPTName', 'projectName' ),
	'add_new'            => _x( 'Add New CPTName', 'projectName', 'projectName' ),
	'add_new_item'       => __( 'Add New CPTName', 'projectName' ),
	'edit_item'          => __( 'Edit CPTName', 'projectName' ),
	'new_item'           => __( 'New CPTName', 'projectName' ),
	'view_item'          => __( 'View CPTName', 'projectName' ),
	'search_items'       => __( 'Search CPTNames', 'projectName' ),
	'not_found'          => __( 'No CPTNames found', 'projectName' ),
	'not_found_in_trash' => __( 'No CPTNames found in Trash', 'projectName' ),
	'parent_item_colon'  => __( 'Parent CPTName:', 'projectName' ),
	'menu_name'          => __( 'CPTNames', 'projectName' ),
);

$args = array(
	'labels'              => $labels,
	//'show_in_rest'		  => true, // Uncomment to make this accessible to the WP-API (public)
	'hierarchical'        => false,
	'description'         => '',
	//'taxonomies'          => array( 'category' ), // Uncomment to add the use of categories
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-carrot', // https://developer.wordpress.org/resource/dashicons
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'editor' ), // { title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats }
);

register_post_type( 'CPTName', $args );